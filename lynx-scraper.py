#!/bin/env python3
# Copyright (c) 2024 Jacob Dybvald Ludvigsen (contributions@ingeniorskap.no)
# SPDX-License-Identifier: AGPL-3.0-or-later


import requests
import json
import os
import time
import sys


def scrape_lynx(locID):
    """
    Download images of Lynx from NINAs wildlife camera database
    """
    # Base URL to query when iterating through locations,
    # because I couldn't find a generic path to get all images.
    url = "https://viltkamera.nina.no/Media/GetMediaByLokID"

    # Parameters to include in the request, mimicking the UX of
    # clicking a location with the "Lynx" species box checked
    params = {
        "LokalitetID": locID,
        "Fromdate": "01.01.2010",
        "Todate": "06.05.2024",
        "ArtID": 1
        }
    print(params)

    # Send the request
    response = requests.get(url, params=params)

    # Parse the JSON response, containing media metadata and paths
    data = json.loads(response.text)
    print(response)

    # Create a directory to store the images if it doesn't exist
    image_dir = "images"
    os.makedirs(image_dir, exist_ok=True)

    # Loop through the image metadata and download each image
    for media in data:
        filetype = media["FK_MediaTypeID"]
        if filetype == 1:  # only download images
            filename = media["Filnavn"]
            url = f"https://viltkamera.nina.no/Media/{filename}"

            print(f'Getting file {filename}')

            # Make a GET request to download the image
            response = requests.get(url)

            if response.status_code == 200:
                # Write file
                with open(os.path.join(image_dir, filename), "wb") as f:
                    f.write(response.content)

                time.sleep(0.5)  # Wait a bit to avoid overloading server


def main():
    """
    Use:
    lynx-scraper.py <lowest index of location to scrape> <highest index>

    Example:
    lynx-scraper.py 0 5000
    """

    for i in range(int(sys.argv[1]), int(sys.argv[2])):
        try:
            scrape_lynx(str(i))
        except Exception as e:
            print(e)
        time.sleep(1)  # Wait a bit to avoid overloading server


if __name__ == "__main__":
    main()
